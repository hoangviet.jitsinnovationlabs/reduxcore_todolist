import React from 'react'

const Date1 = () => {
    var today=new Date()
    const month = ["January","February","March","April","May","June","July","August","September","October","November","December"];
    
    let monthName = month[today.getMonth()];
    var date = (monthName)+'   '+today.getFullYear();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var dateTime = date+' '+time;
    
  return (
    <div className='date-background'>
        <div className="date">
            <h1>{today.getDate()}</h1>
        </div>
        <div className="month">
          <p>{date}</p>
        </div>



    </div>
  )
}

export default Date1