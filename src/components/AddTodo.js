import React from 'react';
import { useDispatch } from 'react-redux';
import { addTodo } from '../redux/action';
import './todoList.css'
const AddTodo = () => {
    let input;
    const dispatch=useDispatch();
    const addTask = (e) => {
        e.preventDefault();  
            if(!input.value.trim()) return ;
            dispatch(addTodo(
                    {name: input.value}
                )
            )
    }
    console.log("hahaha")
    return (
        <div className="input">
            <form onSubmit={addTask}>
                <input ref={node => input = node}/>
                <button style={{ backgroundColor: "rgb(94 65 190)", color:"white"}}
                         type="submit">
                    Add Todo
                </button>
            </form>
        </div>
    )
}

export default AddTodo;