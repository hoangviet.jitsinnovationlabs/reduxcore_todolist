import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useState, useEffect } from 'react'
import { deleteTodo, completedTodo, getTodoAsync } from '../redux/action';
import './todoList.css'
import Date from './date';
const TodoList = () => {
    const dispatch = useDispatch();
    const listTodos = useSelector(state => state.todos)
    const handleDelete = (id) => {
        dispatch(deleteTodo({
            id: id,
        }))
    }
    const handleCompleted = (id) => {
        dispatch(completedTodo({
            id: id,
        }))
    }
    return (
        <div className='todoList'>
            <Date />
            <ul className='todo-box'>
                {
                    listTodos.map((item, index) => {
                        return <div key={index} className="listTodos">
                            <li
                                className={item.completed == false ? "todo" : "completed todo"}
                                style={{ display: "inline-block", background: "" }}
                            >{item.name}</li>
                            <div className='state'>
                                <button
                                    className='button-completed'
                                    onClick={() => { handleCompleted(item.id) }}
                                >completed</button>
                                <button
                                    className='button-delete'
                                    onClick={() => handleDelete(item.id)}>delete</button>
                                <br></br>
                            </div>
                        </div>


                    })


                }
            </ul>
        </div>
    )
}

export default TodoList;