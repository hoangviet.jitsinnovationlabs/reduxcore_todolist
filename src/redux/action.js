import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
export const addTodo = (data)=>{
    return {
        type: 'todos/addTodo',
        payload: data
    }
}
export const deleteTodo=(data)=>{
    return{
        type: 'todos/deleteTodo',
        payload: data
    }
}
export const completedTodo=(data)=>{
    return{
        type: 'todos/completedTodo',
        payload: data
    }
}
// export const getTodoAsync= ( async dispatch =>{// bởi vì action getdoto phải mất thời gian để đợi, mà redux không muốn đợi, nên phải dùng
//                                         // middle ware redux thunk để bắt nó đợi fetchdata
//     const response= await fetch('http://localhost:30001/todoList');
//     if(response.ok){
//         const todoList= await response.json();
//         console.log(todoList);
//         return dispatch({
//             type: 'todoList/addAsyncTodo',
//             payload: {todoList}
//         })
//     }
//     // redux thunk sẽ trả về 1 plain javascript function( hay là pure function)
// })