
const iniState = {
    todos: [
        { id: 1, name: "Học ăn", completed: false },
        { id: 2, name: "Học nói", completed: true },
        { id: 3, name: "Học gói", completed: false },
    ]

}
const rootReducer = (state = iniState, action) => {
    
    switch (action.type) {
        case 'todos/addTodo':
            return {
                ...state,
                todos: [
                    ...state.todos, { id: Date.now(), name: action.payload.name, completed: false },
                ]
            }
        case 'todos/deleteTodo':
            return {
                ...state,
                todos: [
                    ...state.todos.filter(item => item.id !== action.payload.id)
                ]
            }
        case 'todos/completedTodo':
            return {
                    ...state.todos.map(item=>{if(item.id==action.payload.id) item.completed=!item.completed}),
                    todos:[...state.todos] 
            }
        default: return state
    }

}
export default rootReducer